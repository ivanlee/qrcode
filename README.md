# QRCode app

Yet another QRCode generator app, optimized to be used on the commandline when
connected to a remote server to create/share QR codes.

A public version of this is running at https://qr.ivan.tools - feel free to use it.

This was really built for Wireguard config sharing, so here is an example of how this can be used:

```sh
# Imagine doing this in a terminal window while connected to a remote server...
$> echo "[Interface] \n# your wireguard config\n" > wg0.conf
$> curl -X POST --data-binary @wg0.conf https://qr.ivan.tools/url
{"png":"https://qr.ivan.tools/png/qRvG","view":"https://qr.ivan.tools/view/qRvG"}
# Now, you have 10 minutes to type this out in a browser to get the QR code to be used/shared.
```

## Security

When using the `/url` feature, the content is stored on the server for 10
minutes via Redis. I do not log anything in the server apart from standard
access log of the Caddy server (which will have the URL and your IP address),
and the Redis database is set to be non-persistent. The logging is just so that
I have an idea of how many people are using this and nothing more, and I have
no intention of checking out what's in the Redis database.

If this is still a concern for you, feel free to run the app yourself, which is
easy to do with the provided `docker-compose.yml` file.
