import aioredis
import base64
from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.responses import PlainTextResponse
from fastapi.responses import Response
from io import BytesIO
import hashlib
import qrcode
from starlette.datastructures import URLPath
import uuid

app = FastAPI()
redis = aioredis.from_url('redis://redis:6379', decode_responses=True)

@app.get('/', response_class=HTMLResponse)
async def root():
    return """Go to
<a href="https://gitlab.com/ivanlee/qrcode">https://gitlab.com/ivanlee/qrcode</a>
for more instructions on how to use this"""

def generate_png(content: str):
    img = qrcode.make(content)
    b = bytes()
    f = BytesIO(b)
    img.save(f, format='png')
    try:
        return f.getvalue()
    finally:
        f.close()

async def generate_code_id(content: str):
    attempt = 0
    while attempt < 5:
        digest = hashlib.sha256(str(uuid.uuid4()).encode()).digest()
        code_id = base64.urlsafe_b64encode(digest)[:4].decode('utf-8')
        # Set it if it's unused, and auto expire after 10 min...
        if await redis.set(code_id, content, ex=600, nx=True):
            # Write success!
            return code_id
        attempt += 1
    raise Exception(f'Failed to generate code_id after {attempt} attempts')

@app.get('/url', response_class=HTMLResponse)
async def url_get(content: str, request: Request):
    code_id = await generate_code_id(content)
    png_url = URLPath(f'/png/{code_id}').make_absolute_url(request.base_url)
    view_url = URLPath(f'/view/{code_id}').make_absolute_url(request.base_url)
    return f"""<img src="{png_url}">
<br />
<a href="{view_url}">{view_url}</a>"""

@app.post('/url')
async def url_post(request: Request):
    content = await request.body()
    code_id = await generate_code_id(content)
    png_url = URLPath(f'/png/{code_id}').make_absolute_url(request.base_url)
    view_url = URLPath(f'/view/{code_id}').make_absolute_url(request.base_url)
    return {'png': png_url, 'view': view_url}

@app.get('/png')
async def png_get(content: str):
    return Response(generate_png(content), media_type='image/png')

@app.post('/png')
async def png_post(request: Request):
    content = await request.body()
    return Response(generate_png(content), media_type='image/png')

@app.get('/png/{code_id}')
async def png_id(code_id: str):
    content = await redis.get(code_id)
    if not content:
        response.status_code = 404
        return ''
    return Response(generate_png(content), media_type='image/png')

@app.get('/view', response_class=HTMLResponse)
async def view(content: str):
    b64_png = base64.b64encode(generate_png(content)).decode('UTF-8')
    return f'<img src="data:image/png;base64,{b64_png}">'

@app.get('/view/{code_id}', response_class=HTMLResponse)
async def view_id(code_id: str, response: Response):
    content = await redis.get(code_id)
    if not content:
        response.status_code = 404
        return ''
    b64_png = base64.b64encode(generate_png(content)).decode('UTF-8')
    return f'<img src="data:image/png;base64,{b64_png}">'

