#!/bin/sh

# Just an example of how to share a Wireguard config (in wg0.conf) with
# someone.  The link generated is valid for 10 minutes, but the url should be
# short enough to type this out elsewhere to download the png to share to
# others.

curl -X POST --data-binary wg0.conf http://localhost:8080/url

# Just add an extra line, to prettify output
echo ""
